package herb.project

class Model{

    var name:String? = null
    var image:String? = null
    var detail:String? = null
    var name_h:String? = null
    var hurt:String? = null
    var image_h:String? = null
    var herb:String? = null

    constructor():this("","","","","","",""){

    }

    constructor(name:String,image:String,detail:String,name_h:String,hurt:String,image_h:String,herb:String){
        this.name = name
        this.image = image
        this.detail = detail
        this.name_h = name_h
        this.hurt = hurt
        this.image_h = image_h
        this.herb = herb

    }
}