package herb.project

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_card_herb.view.*

class CardHerb : Fragment() {
    lateinit var database: FirebaseDatabase
    lateinit var myRef:DatabaseReference
    lateinit var recyclerView: RecyclerView
    lateinit var query:Query
    lateinit var arrayList:ArrayList<Model>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_card_herb, container, false)

        arrayList = ArrayList<Model>()
        database = FirebaseDatabase.getInstance()
        myRef = database.getReference("herb")
        myRef.keepSynced(true);



        recyclerView = view.findViewById(R.id.recycle_type_herb)
        recyclerView.layoutManager = LinearLayoutManager(context)

       /* view.search_card_herb.visibility = View.GONE
        var click = 0
        view.search_herb.setOnClickListener {
            if(click == 0) {
                view.search_card_herb.visibility = View.VISIBLE
                view.image_type_picture.visibility = View.GONE
                click = 1
            }
            else if(click > 0){
                view.search_card_herb.visibility = View.GONE
                view.image_type_picture.visibility = View.VISIBLE
                onStart()
                click = 0
            }


        }*/

        view.btn_back.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,HomeScreen())
                .addToBackStack(null)
                .commit()
        }

        view.search_card_herb.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(!s.toString().isEmpty()){
                    search(s.toString())
                }
                else{
                    search("")
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        if(isOnline(context!!) != true){
            val snackbar = Snackbar.make(activity!!.findViewById(android.R.id.content),
                "Not connecting to internet",Snackbar.LENGTH_LONG)
                .setAction("close",object :View.OnClickListener{
                    override fun onClick(p0: View?) {
                    }

                })
            snackbar.show()
        }

        return view
    }
    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
    override fun onStart() {
        super.onStart()

        val option = FirebaseRecyclerOptions.Builder<Model>()
            .setQuery(myRef,Model::class.java)
            .build()

        val firebaseRecycleAdapter = object:FirebaseRecyclerAdapter<Model,MyViewHolder>(option){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
                val itemView = LayoutInflater.from(context).inflate(R.layout.card_herb,parent,false)
                return MyViewHolder(itemView)
            }

            override fun onBindViewHolder(holder: MyViewHolder, position: Int, model: Model) {
                val placeid = getRef(position).key.toString()
                myRef.child(placeid).addValueEventListener(object :ValueEventListener{
                    override fun onCancelled(p0: DatabaseError) {
                        Toast.makeText(context, "Error Occurred "+ p0.toException(), Toast.LENGTH_SHORT).show()                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        holder.txt_name.setText(model.name)
                        holder.cardView.setOnClickListener {
                            fragmentManager!!.beginTransaction()
                                .replace(R.id.frame_layout,DetailScreen.newInstance(placeid))
                                .addToBackStack(null)
                                .commit()

                        }
                        Picasso.get().load(model.image).centerCrop().fit().into(holder.img_herb)

                    }

                })
            }

        }

        recyclerView.adapter = firebaseRecycleAdapter
        firebaseRecycleAdapter.startListening()

    }
    private fun search(s:String){
      if(!s.equals("")) {
          query = database.getReference("herb").orderByChild("name")
              .startAt(s)
              .endAt(s + "\uf8ff")
          query.addValueEventListener(object : ValueEventListener {
              override fun onCancelled(p0: DatabaseError) {

              }

              override fun onDataChange(p0: DataSnapshot) {
                  if (p0.hasChildren()) {
                      arrayList.clear()
                      for (dss: DataSnapshot in p0.children) {
                          val Model = dss.getValue(Model::class.java)
                          arrayList.add(Model!!)
                      }

                      val myAdapter = MyAdapter(context!!.applicationContext, arrayList)
                      myAdapter.onItemClick = ::onclick
                      recyclerView.adapter = myAdapter
                      myAdapter.notifyDataSetChanged()
                  }
              }

          })
      }else{
          onStart()
      }
    }
    private fun onclick(data:String){
       val query:Query = database.getReference("herb").orderByChild("name").equalTo(data)
        query.keepSynced(true)
        query.addValueEventListener(object:ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                for(childSnapshot:DataSnapshot in p0.children){
                    fragmentManager!!.beginTransaction()
                        .replace(R.id.frame_layout,DetailScreen.newInstance(childSnapshot.key.toString()))
                        .addToBackStack(null)
                        .commit()
                    view!!.search_card_herb.text.clear()
                }
            }

        })
    }
    class MyViewHolder(itemView:View?):RecyclerView.ViewHolder(itemView!!){
        internal var txt_name: TextView = itemView!!.findViewById<TextView>(R.id.name_herb)
        internal var img_herb:ImageView = itemView!!.findViewById<ImageView>(R.id.img_card_herb)
        internal var cardView:CardView = itemView!!.findViewById<CardView>(R.id.card_herb_view)
    }

}
