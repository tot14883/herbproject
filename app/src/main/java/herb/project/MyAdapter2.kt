package herb.project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class MyAdapter2(): RecyclerView.Adapter<MyAdapter2.MyAdapterViewHolder>() {

    lateinit var c: Context
    lateinit var arrayList: ArrayList<Model>

    var onItemClick: ((data: String) -> Unit)? = null

    constructor(c: Context, arrayList: ArrayList<Model>) : this() {
        this.c = c
        this.arrayList = arrayList
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapterViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_herb, parent, false)
        return MyAdapterViewHolder(v)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: MyAdapterViewHolder, position: Int) {
        val ModelItem = arrayList.get(position)

        holder.txt_name.setText(ModelItem.name_h)
        Picasso.get().load(ModelItem.image_h).centerCrop().fit().into(holder.img_herb)
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(ModelItem.name_h.toString())
        }
    }

    public class MyAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var txt_name: TextView = itemView!!.findViewById<TextView>(R.id.name_herb)
        internal var img_herb: ImageView = itemView!!.findViewById<ImageView>(R.id.img_card_herb)
        internal var cardView: CardView = itemView!!.findViewById<CardView>(R.id.card_herb_view)
    }
}
