package herb.project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.activity_home_screen.view.*

class HomeScreen : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_home_screen,container,false)

        view.btn_introduce.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,IntroduceScreen())
                .addToBackStack(null)
                .commit()
        }

        view.btn_card_herb.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,CardHerb())
                .addToBackStack(null)
                .commit()
        }

        view.btn_follow_herb.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,HealScreen())
                .addToBackStack(null)
                .commit()
        }

        view.btn_useful_herb.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,ConsultScreen())
                .addToBackStack(null)
                .commit()
        }

        return view

    }

}
