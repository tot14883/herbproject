package herb.project

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_screen.*
import kotlinx.android.synthetic.main.activity_detail_screen.view.*
import java.lang.Exception

class DetailScreen : Fragment() {
    lateinit var database: FirebaseDatabase
    lateinit var myRef: DatabaseReference
    var position = ""
    companion object{
        fun newInstance(position:String) = DetailScreen().apply{
             arguments = Bundle().apply {
                 putString("position",position)
             }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = arguments?.getString("position")?:""
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_detail_screen,container,false)
        view.progressbar_1.visibility = View.VISIBLE

        database = FirebaseDatabase.getInstance()
        myRef = database.getReference("herb").child(position)
        myRef.keepSynced(true);
        view.btn_back.visibility = View.GONE

        myRef.addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(context, "Error Occurred "+ p0.toException(), Toast.LENGTH_SHORT).show()
            }
            override fun onDataChange(p0: DataSnapshot) {
                val get = p0.getValue(Model::class.java)
                Picasso.get().load(get?.detail).fit().into(detail_img,object: com.squareup.picasso.Callback {
                    override fun onSuccess() {
                        view.progressbar_1.visibility = View.GONE
                        view.btn_back.visibility = View.VISIBLE
                    }

                    override fun onError(e: Exception?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
            }
        })

        if(isOnline(context!!) != true){
            val snackbar = Snackbar.make(activity!!.findViewById(android.R.id.content),
                "Not connecting to internet", Snackbar.LENGTH_LONG)
                .setAction("close",object :View.OnClickListener{
                    override fun onClick(p0: View?) {
                    }

                })
            snackbar.show()
        }

        view.btn_back.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,HomeScreen())
                .addToBackStack(null)
                .commit()
        }
        return view

    }
    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}
