package herb.project

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_herb_for_heal_screen.view.*

class HerbForHealScreen : Fragment() {
    var positionHurt:String = ""

    lateinit var database: FirebaseDatabase
    lateinit var myRef: Query
    lateinit var myHurt:DatabaseReference
    lateinit var recyclerView: RecyclerView
    lateinit var arrayList:ArrayList<Model>

    companion object{
        fun newInstance(position:String) = HerbForHealScreen().apply {
           arguments = Bundle().apply {
               putString("position",position)
           }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        positionHurt = arguments?.getString("position")?:""
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_herb_for_heal_screen,container,false)

        arrayList = ArrayList<Model>()


        database = FirebaseDatabase.getInstance()
        Log.d("ID",positionHurt)
        myRef = database.getReference("herb").orderByChild(positionHurt).equalTo("Have")
        myRef.keepSynced(true);

        var click = 0

      /*  view.search_heal_herb_view_detail.setOnClickListener {
            if(click == 0) {
                view.search_card_herb_heal_detail.visibility = View.VISIBLE
                view.text_header_topic.visibility = View.GONE
                click = 1
            }
            else if(click > 0){
                view.search_card_herb_heal_detail.visibility = View.GONE
                view.text_header_topic.visibility = View.VISIBLE
                onStart()
                click = 0
            }
        }*/

        view.search_card_herb_heal_detail.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if(!p0.toString().isEmpty()){
                    search(p0.toString())
                }
                else{
                    search("")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })

        myHurt = database.getReference("hurt").child(positionHurt)
        myHurt.keepSynced(true)

        myHurt.addValueEventListener(object:ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                val get = p0.getValue(Model::class.java)
                view.search_card_herb_heal_detail.hint = get!!.name_h
                //   view.text_header_topic.setText(get!!.name_h)


            }

        })

        recyclerView = view.findViewById(R.id.recycle_view_herb_heal)
        recyclerView.layoutManager = LinearLayoutManager(context)

        if(isOnline(context!!) != true){
            val snackbar = Snackbar.make(activity!!.findViewById(android.R.id.content),
                "Not connecting to internet", Snackbar.LENGTH_LONG)
                .setAction("close",object :View.OnClickListener{
                    override fun onClick(p0: View?) {
                    }

                })
            snackbar.show()
        }
        view.btn_back.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,HealScreen())
                .addToBackStack(null)
                .commit()
        }
        return view
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun onStart() {
        super.onStart()
        val option = FirebaseRecyclerOptions.Builder<Model>()
            .setQuery(myRef,Model::class.java)
            .build()

        val firebaseRecycleAdapter = object: FirebaseRecyclerAdapter<Model,MyViewHolder>(option){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):MyViewHolder {
                val itemView = LayoutInflater.from(context).inflate(R.layout.card_herb,parent,false)
                return MyViewHolder(itemView)
            }

            override fun onBindViewHolder(holder: MyViewHolder, position: Int, model: Model) {
                val placeid = getRef(position).key.toString()
                    Log.d("Yo",placeid)
                    holder.txt_name.setText(model.name)
                    holder.cardView.setOnClickListener {
                        fragmentManager!!.beginTransaction()
                            .replace(R.id.frame_layout, DetailScreen.newInstance(placeid))
                            .addToBackStack(null)
                            .commit()

                    }
                    Picasso.get().load(model.image).centerCrop().fit().into(holder.img_herb)

            }

        }
        recyclerView.adapter = firebaseRecycleAdapter
        firebaseRecycleAdapter.startListening()

    }

    fun search(s:String){

        if(!s.equals("")) {
            val query: Query = database.getReference("herb").orderByChild("name")
                .startAt(s)
                .endAt(s + "\uf8ff")
                query.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(p0: DataSnapshot) {
                            if (p0.hasChildren()) {
                                arrayList.clear()
                                for (dss: DataSnapshot in p0.children) {
                                    if(dss.child(positionHurt).exists()) {
                                        val Model = dss.getValue(Model::class.java)
                                        arrayList.add(Model!!)
                                    }

                                }

                                val myAdapter = MyAdapter(context!!.applicationContext, arrayList)
                                myAdapter.onItemClick = ::onclick
                                recyclerView.adapter = myAdapter
                                myAdapter.notifyDataSetChanged()
                            }
                }

            })
        }
        else{
            onStart()
        }
    }
    private fun onclick(s:String){
        val query:Query = database.getReference("herb").orderByChild("name").equalTo(s)
        query.keepSynced(true)
        query.addValueEventListener(object:ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                for(childSnapshot:DataSnapshot in p0.children){
                    fragmentManager!!.beginTransaction()
                        .replace(R.id.frame_layout,DetailScreen.newInstance(childSnapshot.key.toString()))
                        .addToBackStack(null)
                        .commit()
                    view!!.search_card_herb_heal_detail.text.clear()
                }
            }

        })
    }

    class MyViewHolder(itemView:View?):RecyclerView.ViewHolder(itemView!!){
        internal var txt_name: TextView = itemView!!.findViewById<TextView>(R.id.name_herb)
        internal var img_herb: ImageView = itemView!!.findViewById<ImageView>(R.id.img_card_herb)
        internal var cardView: CardView = itemView!!.findViewById<CardView>(R.id.card_herb_view)
    }
}
