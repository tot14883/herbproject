package herb.project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_consult_screen.view.*

class ConsultScreen : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_consult_screen, container, false)
        view.btn_back.setOnClickListener {
            fragmentManager!!.beginTransaction()
                .replace(R.id.frame_layout,HomeScreen())
                .addToBackStack(null)
                .commit()
        }

        return view

    }
}
